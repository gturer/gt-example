// stdlib functionality
// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TString.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
// Add a submodule for JetSelectionHelper
#include "JetSelectionHelper/JetSelectionHelper.h"

int main() {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );


  // add jet selection helper
  JetSelectionHelper jet_selector;

  // make histograms for storage
  TH1D *h_njets_raw = new TH1D("h_njets_raw","",20,0,20);

  TH1D *h_njets_good = new TH1D("h_njets_good","",20,0,20);
  //TH1D *h_mjj_raw = new TH1D("h_mjj_raw","",100,0,500);

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    // std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    // make temporary vector of jets for those which pass selection
    std::vector<xAOD::Jet> jets_raw;
    std::vector<xAOD::Jet> jets_good;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      // std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;

      jets_raw.push_back(*jet);

      if(jet_selector.isJetGood(jet)) {
        jets_good.push_back(*jet);
      }

    }

    // fill the analysis histograms accordingly
    h_njets_raw->Fill( jets_raw.size() );

    h_njets_good->Fill( jets_good.size() );

    // counter for the number of events analyzed thus far
    count += 1;
  }

  // open TFile to store the analysis histogram output
  TFile *fout = new TFile("myOutputFile.root","RECREATE");

  h_njets_raw->Write();

  //h_mjj_raw->Write();

  fout->Close();

   /************************************************************/
  /****************    Draw and write histos    ***************/ 
  
  TString outputfile = "../output_of_analysisPayload.pdf(";
  TString outputfile_last = "../output_of_analysisPayload.pdf)";

  TCanvas c1 = TCanvas("canv1","canv1",0,0,800,600);
  c1.cd();
  h_njets_raw->Draw();
  c1.Print(outputfile);

  // TCanvas c2 = TCanvas("canv2","canv2",0,0,800,600);
  // c2.cd();	   
  // h_dijet_inv_mass->Draw();
  // c2.Print(outputfile);

  TCanvas c3 = TCanvas("canv3","canv3",0,0,800,600);
  c3.cd();
  h_njets_good->Draw();
  c3.Print(outputfile_last);
  
  c1.Close();
  //c2.Close();
  c3.Close();


  // exit from the main function cleanly
  return 0;
}